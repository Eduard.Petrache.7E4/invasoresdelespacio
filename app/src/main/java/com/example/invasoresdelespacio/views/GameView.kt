package com.example.invasoresdelespacio.views

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.view.MotionEvent
import android.view.SurfaceView
import com.example.invasoresdelespacio.models.Enemy
import com.example.invasoresdelespacio.models.Player
import com.example.invasoresdelespacio.models.Shot
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@SuppressLint("ViewConstructor")
class GameView(context: Context, private val size: Point) : SurfaceView(context) {
    private var canvas = Canvas()
    private val paint = Paint()
    private val player = Player(context, size.x, size.y)
    private var playing = true
    private val enemies = createEnemies(context, size.x, size.y)
    private var shots = mutableListOf<Shot>()
    private var lastTouchX = 0f
    private var gameState = GameState.RUNNING

    enum class GameState {
        RUNNING,
        GAME_OVER
    }

    private fun createEnemies(context: Context, screenX: Int, screenY: Int): MutableList<Enemy> {
        val enemies = mutableListOf<Enemy>()
        for (i in 0 until 3) {
            for (j in 0 until 5) {
                val enemy = Enemy(context, screenX, screenY)
                enemy.positionX = j * enemy.width.toInt()
                enemy.positionY = (i * enemy.height.toInt()) + enemy.height.toInt()
                enemies.add(enemy)
            }
        }
        return enemies
    }

    init {
        startGame()
    }

    private fun startGame() {
        CoroutineScope(Dispatchers.Main).launch {
            while (playing) {
                draw()
                update()
                delay(10)
            }
        }
    }

    private fun draw() {
        holder.surface?.let { surface ->
            if (surface.isValid) {
                canvas = holder.lockCanvas()
                canvas.drawColor(Color.BLACK)
                if (gameState == GameState.RUNNING) {
                    drawRunningGame()
                } else {
                    drawGameOver()
                }
                holder.unlockCanvasAndPost(canvas)
            }
        }
    }

    private fun drawRunningGame() {
        canvas.drawBitmap(player.bitmap, player.positionX.toFloat(), (size.y - player.height), paint)
        shots.forEach { shot ->
            canvas.drawBitmap(shot.bitmap, shot.positionX, shot.positionY, paint)
        }
        enemies.forEach { enemy ->
            canvas.drawBitmap(enemy.bitmap, enemy.positionX.toFloat(), enemy.positionY.toFloat(), paint)
        }
    }

    private fun drawGameOver() {
        paint.color = Color.WHITE
        paint.textSize = 80f
        paint.textAlign = Paint.Align.CENTER
        canvas.drawText(
            if (enemies.isEmpty()) "YOU WIN!" else "GAME OVER",
            size.x / 2f,
            size.y / 2f,
            paint
        )
    }

    private fun update() {
        updateEnemies()
        player.updatePlayer()
        updateShots()

        if (enemies.isEmpty()) {
            gameState = GameState.GAME_OVER
        }
    }

    private fun updateEnemies() {
        enemies.forEach { enemy ->
            enemy.updateEnemy()
            if (enemy.positionY + enemy.height > size.y) {
                enemy.reset()
            }
            if (Rect.intersects(player.getRect(), enemy.getRect())) {
                gameState = GameState.GAME_OVER
            }
        }
    }

    private fun updateShots() {
        shots.forEach { shot ->
            shot.updateShot()
            if (shot.positionY + shot.height < 0) {
                shots.remove(shot)
            } else {
                val hitEnemy = enemies.find { enemy -> shot.getRect().intersect(enemy.getRect()) }
                if (hitEnemy != null) {
                    shots.remove(shot)
                    enemies.remove(hitEnemy)
                }
            }
        }
    }


    private fun movePlayer(x: Float) {
        val deltaX = x - lastTouchX
        player.positionX += deltaX.toInt()
        player.positionX = player.positionX.coerceIn(0, size.x - player.width.toInt())
        lastTouchX = x
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        event?.let {
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    lastTouchX = event.x
                    shoot()
                }
                MotionEvent.ACTION_MOVE -> {
                    movePlayer(event.x)
                }
            }
        }
        return true
    }

    private fun shoot() {
        if (shots.none()) {
            shots.add(player.shoot()!!)
        }
    }

    fun pause() {
        playing = false
    }

    fun resume() {
        playing = true
        startGame()
    }
}

