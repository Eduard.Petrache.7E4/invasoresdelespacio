package com.example.invasoresdelespacio.fragments

import android.graphics.Point
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.fragment.app.Fragment
import com.example.invasoresdelespacio.views.GameView

class GameFragment : Fragment() {
    private lateinit var gameView: GameView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val display = requireActivity().windowManager.defaultDisplay
        val size = Point().apply { display.getSize(this) }
        gameView = GameView(requireContext(), size)
        val game = FrameLayout(requireContext()).apply {
            addView(gameView, FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT)
        }
        return game
    }

    override fun onPause() {
        super.onPause()
        gameView.pause()
    }

    override fun onResume() {
        super.onResume()
        gameView.resume()
    }
}
