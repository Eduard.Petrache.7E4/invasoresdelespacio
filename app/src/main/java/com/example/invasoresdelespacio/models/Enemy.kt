package com.example.invasoresdelespacio.models

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Rect
import com.example.invasoresdelespacio.R
import kotlin.random.Random

class Enemy(context: Context, private val screenX: Int, private val screenY: Int) {
    var bitmap: Bitmap = BitmapFactory.decodeResource(context.resources, R.drawable.enemy)
    val width = screenX / 10f
    val height = screenY / 10f
    var positionX = screenX / 2
    var positionY = screenY / 4
    var speed = 5

    init {
        bitmap = Bitmap.createScaledBitmap(bitmap, width.toInt(), height.toInt(), false)
    }

    fun updateEnemy() {
        positionX += speed
        if (positionX <= 0 || positionX + width >= screenX) {
            speed = -speed
            positionY += height.toInt()
        }
    }

    fun reset() {
        positionX = Random.nextInt((screenX - width).toInt())
        positionY = (-height).toInt()
    }

    fun getRect(): Rect = Rect(
        positionX,
        positionY,
        (positionX + width).toInt(),
        (positionY + height).toInt()
    )
}
