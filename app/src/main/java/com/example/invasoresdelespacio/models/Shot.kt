package com.example.invasoresdelespacio.models

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Rect
import com.example.invasoresdelespacio.R

class Shot(context: Context, startX: Float, private val screenY: Int) {
    var bitmap: Bitmap = BitmapFactory.decodeResource(context.resources, R.drawable.shot)
    val width = 120f
    val height = 240f
    var positionX = startX - width / 2
    var positionY = screenY - height
    var speed = -20

    init {
        bitmap = Bitmap.createScaledBitmap(bitmap, width.toInt(), height.toInt(), false)
    }

    fun updateShot() {
        positionY += speed
    }

    fun getRect(): Rect = Rect(
        positionX.toInt(),
        positionY.toInt(),
        (positionX + width).toInt(),
        (positionY + height).toInt()
    )
}
