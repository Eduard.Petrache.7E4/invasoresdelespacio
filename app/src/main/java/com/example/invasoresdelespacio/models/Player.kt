package com.example.invasoresdelespacio.models

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Rect
import com.example.invasoresdelespacio.R

class Player(val context: Context, screenX: Int, private val screenY: Int) {
    var bitmap: Bitmap = BitmapFactory.decodeResource(context.resources, R.drawable.spaceship)
    val width = screenX / 10f
    val height = screenY / 10f
    var positionX = screenX / 2
    var speed = 0

    init {
        bitmap = Bitmap.createScaledBitmap(bitmap, width.toInt(), height.toInt(), false)
    }

    fun updatePlayer() {
        positionX += speed
    }

    fun shoot(): Shot? = Shot(context, positionX + width / 2, screenY)

    fun getRect(): Rect = Rect(
        positionX.toInt(),
        (screenY - height).toInt(),
        (positionX + width).toInt(),
        screenY
    )
}
