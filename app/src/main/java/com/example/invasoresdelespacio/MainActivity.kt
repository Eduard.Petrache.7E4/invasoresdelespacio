package com.example.invasoresdelespacio

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.invasoresdelespacio.fragments.GameFragment

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.hide()
        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .add(R.id.fragment_container, GameFragment())
                .commit()
        }
    }
}
